package model.vo;

import model.data_structures.MyList;

public class Chicago {
	//Atributos
	public MyList<Compania> city;
	public MyList<CompaniaServicios> companiaServicios;
	public MyList<Servicio> servicios;
	
	//Constructor
	public Chicago(){
		city = new MyList<Compania>();
		servicios = new MyList<Servicio>();
		companiaServicios = new MyList<CompaniaServicios>();
	}
	
	//Metodos
	public MyList<Compania> darCompanias(){
		return city;
	}
	
	public MyList<CompaniaServicios> darCompaniaServicios(){
		return companiaServicios;
	}
	
	public MyList<Servicio> darServicios(){
		return servicios;
	}
	
	/**
	 * Busca la compania con el nombre dado por parametro
	 * @param company Nombre de la compania que se quiere
	 * @return compania con el nombre dado por parametro
	 */
	public Compania buscarCompania(String company)
	{
		Compania buscada = null;
		city.listing();
		while(true)
		{
			if(city.getCurrent().getNombre().equals(company))
			{
				buscada=city.getCurrent();
				break;
			}
			city.avanzar();
			try 
			{
				city.getCurrent();
			} 
			catch (Exception e) 
			{
				break;
			}
		}
		return buscada;
	}
	
	/**
	 * Busca un taxi segun su id
	 * @param taxiId identificador asociado al taxi que se busca
	 * @return Taxi con el id dado por parametro
	 * 			null si no encuentra el taxi
	 */
	public Taxi buscarTaxi(String taxiId)
	{
		Taxi buscado = null;
		city.listing();
		while(city.getCurrent()!=null)
		{	
			buscado = city.getCurrent().buscarTaxi(taxiId);
			if(buscado!=null)
				break;
			city.avanzar();
			try
			{
				city.getCurrent();
			} 
			catch (Exception e) 
			{
				break;
			}
		}
		return buscado;	
	}
	
	/**
	 * Lista de servicios prestados en un rango dado por parametro
	 * @param rango Fecha y hora de inicio y fin en la que se encuentran los servicios
	 * @return Cola con los servicios prestados en el rango dado
	 */
	public MyList<Servicio> serviciosEnRango(RangoFechaHora rango)
	{
		MyList<Servicio> enRango= new MyList<Servicio>();

		String inicio= rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String fin= rango.getFechaFinal()+"T"+rango.getHoraFinal();

		Servicio compararRango = new Servicio(inicio, fin);

		boolean empezo = false;
		servicios.listing();
		while(true)
		{
			try {
				servicios.getCurrent();
			} catch (Exception e) {
				break;
				// TODO: handle exception
			}
			Servicio actual=servicios.getCurrent();
			if(empezo)
			{
				//comparar fecha inicial del servicio con fecha final del rango
				if(!actual.getStartTime().before(compararRango.getEndTime()))
					break;
			}
			int temp=actual.compareTo(compararRango);
			if(temp == -1 || temp == 0 )
			{
				empezo=true;
				if(actual.getEndTime().before(compararRango.getEndTime()))
					enRango.add(servicios.getCurrent());
			}
			servicios.avanzar();
		}
		return enRango;
	}
}