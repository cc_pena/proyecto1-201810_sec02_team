package model.logic;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.MyList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Chicago;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";

	private Chicago chicago;

	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		// TODO Auto-generated method stub
		JsonParser parser = new JsonParser();
		File[] array = new File[1];
		chicago = new Chicago();
		if(direccionJson.equals(DIRECCION_LARGE_JSON))
			array = new File(direccionJson).listFiles();
		else
			array[0] = new File(direccionJson);
		try {
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			for(int a = 0; a < array.length; a++)
			{
				JsonArray arr= (JsonArray) parser.parse(new FileReader(array[a]));
				Compania compa�ia = null;
				/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
				for (int i = 0; arr != null && i < arr.size(); i++)
				{
					JsonObject obj= (JsonObject) arr.get(i);
					/* Mostrar un JsonObject (Servicio taxi) */

					String company = obj.get("company") == null? "NaN": obj.get("company").getAsString();
					company = company.replaceAll("\\P{L}+", "");
					compa�ia = chicago.city.get(new Compania(company));
					if(compa�ia == null)compa�ia = chicago.city.addInOrder(new Compania(company));

					/* Obtener la propiedad taxi_id de un taxi (String)*/
					String taxi_id = obj.get("taxi_id") == null? "NaN": obj.get("taxi_id").getAsString();
					Taxi taxi = compa�ia.taxiList.get(new Taxi(taxi_id,company));
					if(taxi == null)taxi = compa�ia.taxiList.add(new Taxi(taxi_id,company));

					String trip_id = obj.get("trip_id") == null? "NaN": obj.get("trip_id").getAsString();
					double dropoff_census_tract = obj.get("dropoff_census_tract") == null? 0: obj.get("dropoff_census_tract").getAsDouble();
					double dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude") == null? 0: obj.get("dropoff_centroid_latitude").getAsDouble();

					JsonElement drop = obj.get("dropoff_centroid_location");
					String dropoff_type = drop == null? "NaN": drop.getAsJsonObject().get("type").getAsString();
					double droplat = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
					double droplong = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();

					double dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude") == null? 0: obj.get("dropoff_centroid_longitude").getAsDouble();
					int dropoff_community_area = obj.get("dropoff_community_area") == null? 0: obj.get("dropoff_community_area").getAsInt();

					double extras = obj.get("extras") == null? 0: obj.get("extras").getAsDouble();
					double fare = obj.get("fare") == null? 0: obj.get("fare").getAsDouble();
					String payment_type = obj.get("payment_type") == null? "NaN": obj.get("payment_type").getAsString();

					double pickup_census_tract = obj.get("pickup_census_tract") == null? 0: obj.get("pickup_census_tract").getAsDouble();
					double pickup_centroid_latitude = obj.get("pickup_centroid_latitude") == null? 0: obj.get("pickup_centroid_latitude").getAsDouble();

					JsonElement pick = obj.get("pickup_centroid_location");
					String pickup_type = pick == null? "NaN": pick.getAsJsonObject().get("type").getAsString();
					double picklat = pick == null? 0: pick.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
					double picklong = pick == null? 0: pick.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();

					double pickup_centroid_longitude = obj.get("pickup_centroid_longitude") == null? 0: obj.get("pickup_centroid_longitude").getAsDouble();
					int pickup_community_area = obj.get("pickup_community_area") == null? 0: obj.get("pickup_community_area").getAsInt();

					String taxiAutor = taxi_id;
					double tips = obj.get("tips") == null? 0: obj.get("tips").getAsDouble();
					double tolls = obj.get("tolls") == null? 0: obj.get("tolls").getAsDouble();

					String trip_end_timestamp = obj.get("trip_end_timestamp") == null? "NaN": obj.get("trip_end_timestamp").getAsString();
					double trip_miles = obj.get("trip_miles") == null? 0: obj.get("trip_miles").getAsDouble();
					int trip_seconds = obj.get("trip_seconds") == null? 0: obj.get("trip_seconds").getAsInt();
					String trip_start_timestamp = obj.get("trip_start_timestamp") == null? "NaN": obj.get("trip_start_timestamp").getAsString();
					double trip_total = obj.get("trip_total") == null? 0: obj.get("trip_total").getAsDouble();

					Servicio serv = new Servicio(trip_id, dropoff_census_tract, dropoff_centroid_latitude, dropoff_type, droplat, droplong, dropoff_centroid_longitude, dropoff_community_area, extras, fare, payment_type, pickup_census_tract, pickup_centroid_latitude, pickup_type, picklat, picklong, pickup_centroid_longitude, pickup_community_area, taxiAutor, tips, tolls, trip_end_timestamp, trip_miles, trip_seconds, trip_start_timestamp, trip_total);

					compa�ia.servicioList.addInOrder(serv);
					taxi.getMisServicios().addInOrder(serv);
					chicago.servicios.addInOrder(serv);

					System.out.println(i); 
					if(i == 700)
					{
						break;
					}
				}
			}
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}

	/**
	 *<pre> Los servicios estan ordenados cronologicamente 
	 *@param rango informacion de la fecha de inicio y fecha fin con la que se deben comparar los servicios
	 *				la informaci�n llega como Strings
	 *@return Cola de servicios que inician y terminan en el rango dado por par�metro
	 *			si no se encuentran servicios en el rango retornara una cola vacia
	 */
	@Override //1A
	public Queue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		Queue<Servicio> enRango= new Queue<Servicio>();
		//Formato fecha: "yyyy-MM-dd'T'HH:mm:ss.SSS" 
		String inicio= rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String fin= rango.getFechaFinal()+"T"+rango.getHoraFinal();
		Servicio compararRango = new Servicio(inicio, fin);
		MyList<Servicio> servicios = chicago.darServicios();
		boolean empezo = false;
		servicios.listing();
		while(true)
		{
			Servicio actual=servicios.getCurrent();
			if(empezo)
			{
				//comparar fecha inicial del servicio con fecha final del rango
				if(!actual.getStartTime().before(compararRango.getEndTime()))
					break;
			}
			int temp=actual.compareTo(compararRango);
			if(temp <= 0)
			{
				empezo=true;
				if(actual.getEndTime().before(compararRango.getEndTime()))
					enRango.enqueue(servicios.getCurrent());
			}
			servicios.avanzar();
			try 
			{
				servicios.getCurrent();
			} 
			catch (Exception e) 
			{
				break;
			}
		}

		return enRango;
	}

	/**
	 * Retorna el taxi que mas servicios presto en un rango de tiempo dado por parametro de una compania dada por parametro
	 * @param rango: Fecha y hora inicial y final en la que ser quiere consultar
	 * @param company: compania de la que se quiere consultar el taxi con mas servicios en rango 
	 * @return El taxi de la compania dada por parametro con que mas servicios presto en un rango de fecha y hora
	 * 				Si no encuentra la compania retorna null
	 */
	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		Taxi taxiMasServicios=null;
		//buscar compania
		Compania nCompany = chicago.buscarCompania(company); 
		if(nCompany!=null)
		{
			MyList<Taxi> taxis = nCompany.getMisTaxis();
			taxis.listing();
			int cuantosServ=0;
			while(true)
			{
				int servDeCurrent = taxis.getCurrent().serviciosIniciadosEnRango(rango).size();
				if(servDeCurrent >=cuantosServ)
				{
					cuantosServ=servDeCurrent;
					taxiMasServicios=taxis.getCurrent();
				}
				taxis.avanzar();
				try 
				{
					taxis.getCurrent();
				} 
				catch (Exception e) 
				{
					break;
				}
			}

		}
		else
			System.err.println("La compa�ia ingresada no existe");

		return taxiMasServicios;
	}

	/**
	 * Retorna la informacion en el rango dado por parametro del taxi con el id por parametro 
	 * @param id Identificador del taxi que se busca
	 * @param rango Tango de fecha y hora del que se quiere ver la informacion del taxi
	 * 			si no encuentra el taxi retorna un objeto de InfoTaxiRango con el id por parametro
	 * 			como id, el rango parametro como rango y con la cadena "No se encontro taxi" como compania
	 */
	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		InfoTaxiRango info = new InfoTaxiRango();
		info.setIdTaxi(id);
		info.setRango(rango);
		Taxi elTaxi = chicago.buscarTaxi(id);
		if(elTaxi!=null)
		{			
			info.setCompany(elTaxi.getCompany());
			info.setPlataGanada(elTaxi.getGananciaEnRango(rango));
			info.setServiciosPrestadosEnRango(elTaxi.serviciosEnRango(rango));
			info.setDistanciaTotalRecorrida(elTaxi.getRecorridoEnRango(rango));
			info.setTiempoTotal(Integer.toString(elTaxi.getTiempoTotalEnRango(rango)));
		}
		else
			info.setCompany("No se encontro el taxi");

		return info;
	}

	@Override //4A
	public MyList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		MyList<RangoDistancia> respuesta= new MyList<RangoDistancia>();
		RangoFechaHora rango = new RangoFechaHora(fecha, fecha, horaInicial, horaFinal);
		MyList<Servicio> serviciosEnRango = chicago.serviciosEnRango(rango);
		
		
		
		
		
		serviciosEnRango.listing();
		int servEnRango=serviciosEnRango.size();int m=0;System.out.println("cuantos servicios "+servEnRango); 
		while(serviciosEnRango.size()>0)
		{
			Servicio actual = serviciosEnRango.getCurrent();
			int inf =(int) actual.getTripMiles();
			if(respuesta.size()!=0)
			{
				boolean anadio=false;
				for(int i=0; i<respuesta.size(); i++)
				{
					RangoDistancia esteRegistro = respuesta.get(i);
					if(esteRegistro.getLimineInferior()<=inf && esteRegistro.getLimiteSuperior()>inf)
					{
						esteRegistro.getServiciosEnRango().add(actual);
						anadio=true;
					}
				}
				if(!anadio)
				{
					RangoDistancia nuevoRango = new RangoDistancia(); 	
					MyList<Servicio> servDeRango = new MyList<Servicio>();
					nuevoRango.setLimineInferior(inf);
					nuevoRango.setLimiteSuperior(inf + 1);
					nuevoRango.setServiciosEnRango(servDeRango);
					respuesta.addInOrder(nuevoRango);
				}
			}
			else 
			{	
				RangoDistancia nuevoRango = new RangoDistancia(); 	
				MyList<Servicio> servDeRango = new MyList<Servicio>();
				nuevoRango.setLimineInferior(inf);
				nuevoRango.setLimiteSuperior(inf + 1);
				nuevoRango.setServiciosEnRango(servDeRango);
				respuesta.addInOrder(nuevoRango);
			}
			serviciosEnRango.delete(actual);
			serviciosEnRango.avanzar();
		}
		return respuesta;
	}

	@Override //1B
	public MyList<Compania> darCompaniasTaxisInscritos() 
	{
		// TODO Auto-generated method stub
		return chicago.city;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		//		comparar inicio del servicio con el start time
		//		comparar final del servicio con el final time 
		Compania com = chicago.city.get(new Compania(nomCompania));
		if(com == null)
		{
			return null;
		}

		//2017-02-01T09:00:00.000

		String inicio= rango.getFechaInicial()+ "T" +rango.getHoraInicio();
		String fin= rango.getFechaFinal()+ "T" +rango.getHoraFinal();

		Servicio compararRango = new Servicio(inicio, fin);

		Taxi mejor = null;
		for(int i = 0; i < com.taxiList.size(); i++)
		{
			Taxi tax = com.taxiList.get(i);
			double max = 0;
			double contador =0;
			for (int j = 0; j < tax.getMisServicios().size(); j++)
			{
				Servicio serv = tax.getMisServicios().get(j);
				if(serv.getEndTime().after(compararRango.getEndTime()))
					break;
				if(!serv.getStartTime().before(compararRango.getStartTime()))
				{
					contador += serv.trip_total;
				}	
			}
			if(contador > max)
			{
				max = contador;
				mejor = tax;
			}
		}
		return mejor;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		ServiciosValorPagado[] arreglo = new ServiciosValorPagado[3];
		arreglo[0] = new ServiciosValorPagado();
		arreglo[1] = new ServiciosValorPagado();
		arreglo[2] = new ServiciosValorPagado();

		String inicio= rango.getFechaInicial()+ "T" +rango.getHoraInicio();
		String fin= rango.getFechaFinal()+ "T" +rango.getHoraFinal();
		int zona = Integer.parseInt(idZona);

		Servicio compararRango = new Servicio(inicio, fin);
		chicago.servicios.listing();
		while(true)
		{
			Servicio serv = chicago.servicios.getCurrent();
			if(serv.getEndTime().after(compararRango.getEndTime()))
				break;
			if(serv.getStartTime().after(compararRango.getStartTime()))
			{
				if(serv.dropoff_community_area != zona && serv.pickup_community_area == zona)
				{
					arreglo[0].getServiciosAsociados().addInOrder(serv);
					arreglo[0].setValorAcumulado(arreglo[0].getValorAcumulado() + serv.trip_total);
				}
				else if(serv.dropoff_community_area == zona && serv.pickup_community_area != zona)
				{
					arreglo[1].getServiciosAsociados().addInOrder(serv);
					arreglo[1].setValorAcumulado(arreglo[0].getValorAcumulado() + serv.trip_total);
				}
				else if(serv.dropoff_community_area == zona && serv.pickup_community_area == zona)
				{
					arreglo[2].getServiciosAsociados().addInOrder(serv);
					arreglo[2].setValorAcumulado(arreglo[0].getValorAcumulado() + serv.trip_total);
				}
			}
			chicago.servicios.avanzar();
			try 
			{
				chicago.servicios.getCurrent();
			} 
			catch (NullPointerException e) 
			{
				break;
			}
		}
		// TODO Auto-generated method stub
		return arreglo;
	}

	@Override //4B
	public MyList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		MyList<ZonaServicios> lista = new MyList<ZonaServicios>();

		String inicio= rango.getFechaInicial()+ "T" +rango.getHoraInicio();
		String fin= rango.getFechaFinal()+ "T" +rango.getHoraFinal();

		Servicio compararRango = new Servicio(inicio, fin);

		chicago.servicios.listing();
		while(true)
		{
			Servicio serv = chicago.servicios.getCurrent();

			if(serv.getEndTime().after(compararRango.getEndTime()))
				break;
			if(serv.getStartTime().after(compararRango.getStartTime()))
			{
				ZonaServicios temp = lista.get(new ZonaServicios(""+serv.pickup_community_area));
				if(temp == null)temp = lista.addInOrder(new ZonaServicios(""+serv.pickup_community_area));
				temp.getFechasServicios().addInOrder(serv);
			}
			chicago.servicios.avanzar();
			try 
			{
				chicago.servicios.getCurrent();
			} 
			catch (NullPointerException e) 
			{
				break;
			}
		}
		return lista;
	}

	@Override //2C
	public MyList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		MyList<CompaniaServicios> respuesta = new MyList<CompaniaServicios>();
		MyList<Compania> companias = chicago.darCompanias();
		int superior = 1;
		int inferior = 0;
		for(int i=0;i<companias.size();i++){
			MyList<Servicio> servicios = companias.get(i).serviciosEnRango(rango);
			if(servicios.size()>=superior || servicios.size()>inferior)
			{
				CompaniaServicios agregar = new CompaniaServicios();
				agregar.setNomCompania(companias.get(i).getNombre());
				agregar.setServicios(servicios);
				respuesta.addInOrder(agregar);
				if(respuesta.size()>n)
				{
					respuesta.delete(respuesta.get(n));
					superior = respuesta.get(0).getServicios().size();
					inferior = respuesta.get(n-1).getServicios().size();

				}
			}
		}
		return respuesta;
	}


	/**
	 * Retorna los taxis mas rentables de cada compania
	 * @return una lista con objetos CompaniaTaxi que son los mas rentables
	 */
	@Override //3C
	public MyList<CompaniaTaxi> taxisMasRentables()
	{
		MyList<CompaniaTaxi> masRentables = new MyList<CompaniaTaxi>();
		for (int i = 0; i < chicago.city.size(); i++) {
			CompaniaTaxi agregar = new CompaniaTaxi();
			Compania comp = chicago.city.get(i);
			agregar.setNomCompania(comp.getNombre());
			agregar.setTaxi(comp.masRetable());
			masRentables.addInOrder(agregar);
		}
		return masRentables;
	}

	@Override //4C
	public Stack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		Stack<Servicio> stack = new Stack<Servicio>();
		Stack<Servicio> retornado = new Stack<Servicio>();
		Taxi taxi = chicago.buscarTaxi(taxiId);
		taxi.getMisServicios().listing();

		double millas = 0;
		int duracion = 0;
		double ganancia = 0;
		Servicio grande = null;

		Date fechainicial = null;
		Date fechafinal = null;
		try 
		{
			fechainicial = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(fecha + "T" + horaInicial);
			fechafinal = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(fecha + "T" + horaFinal);
		} 
		catch (ParseException e){ System.out.println("formato incorrecto"); }

		Date saveinicial= null;
		Date savefinal = null;

		Date tst = null;
		Date otroTst = null;

		for(int i = 0; i < taxi.getMisServicios().size();i++)
		{
			Servicio serv = taxi.getMisServicios().getCurrent();
			try 
			{
				tst = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(serv.trip_start_timestamp);
				otroTst = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(serv.trip_end_timestamp);
				if(!(tst.before(fechainicial) || otroTst.after(fechafinal)))
				{
					stack.push(serv);
					millas += serv.trip_miles;
				}	
			} 
			catch (ParseException e){ System.out.println("formato incorrecto"); }

			if(millas > 10)
			{
				while(!stack.isEmpty())
				{
					Servicio aux = stack.pop();

					String inicialtemp = aux.trip_start_timestamp;
					String finaltemp = aux.trip_end_timestamp;

					tst = null;
					otroTst = null;

					try 
					{
						tst = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(inicialtemp);
						otroTst = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(finaltemp);
						if(saveinicial == null && savefinal == null)
						{
							saveinicial = tst;
							savefinal = otroTst;
						}
						else
						{
							if(tst.before(saveinicial))
								saveinicial = tst;
							if(otroTst.after(savefinal))
								savefinal = otroTst;
						}
					} 
					catch (ParseException e){ System.out.println("formato incorrecto"); }

					duracion += aux.trip_seconds;
					ganancia += aux.trip_total;
				}
				grande = new Servicio(saveinicial.toString(), savefinal.toString(), millas, duracion, ganancia);
				retornado.push(grande);
				duracion = 0; ganancia = 0; millas = 0; saveinicial = null; savefinal = null;
			}
			taxi.getMisServicios().avanzar();
		}
		while(!stack.isEmpty())
		{
			retornado.push(stack.pop());
		}
		return retornado;
	}

}
