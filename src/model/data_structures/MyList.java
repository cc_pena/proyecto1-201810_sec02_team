package model.data_structures;



public class MyList <T extends Comparable<T>> implements LinkedList<T>{

	protected int size;
	protected Node<T> first;
	protected Node<T> lastNode;
	protected Node<T> actualNode;

	public MyList(){
		actualNode = new Node<T>(null);
		first = null;
		lastNode = null;
	}
	
	/**
	 * Agrega un alemento a la lista 
	 * NO agrega elementos repetidos
	 */
	@Override
	public T add(T element) {
		// TODO Auto-generated method stub
		if(get(element) != null){
			return null;
		}
		if(actualNode == null){
			actualNode = new Node<T>(element);
		}
		if(size == 0){
			first = actualNode;
			lastNode = actualNode;
		}
		else{
			lastNode.next = actualNode;
		}
		actualNode.setElement(element);
		size++;
		return actualNode.getElement();
	}

	public boolean delete(T element) {
		if(size() == 0){
			return false;
		}
		else{
			if(first.getElement().compareTo(element) == 0){
				if(first.getNext() == null){
					first.setElement(null);
					size--;
					return true;
				}
				else{
					first = first.getNext();
					size--;
					return true;
				}
			}
			listing();
			while(actualNode.getNext()!= null){
				if(actualNode.getNext().getElement().compareTo(element) == 0)
					break;
				actualNode = actualNode.getNext();
			}
			if(actualNode.getNext()==null){
				return false;
			}
			else{
				actualNode.next = actualNode.getNext().getNext();
				size--;
				return true;
			}
		}
	}

	public T get(T element) {
		listing();
		while(actualNode != null && actualNode.getElement() != null){
			if(actualNode.getElement().compareTo(element)==0){
				return actualNode.getElement();
			}
			if(actualNode.getNext() == null){
				lastNode = actualNode;
			}
			actualNode = actualNode.getNext();
		}
		return null;
	}

	public T trade()
	{
		T elem = first.getElement();
		first = first.next;
		size--;
		return elem;
	}

	/**
	 * Agrega objetos repetidos a la lista de forma ordenada
	 * Esta adaptado para la adici�n de servicios y de compa�ias
	 */
	public T addInOrder(T elem)
	{
		listing();
		Node<T> temp = new Node<T>(elem);
		
		Node<T> antes = null;
		while(actualNode != null && elem.compareTo(actualNode.getElement()) < 0)
		{
			antes = actualNode;
			actualNode = actualNode.next;
		}
		if(antes == null)
		{
			temp.next=actualNode;
			first = temp;
		}
		else
		{
			temp.next = actualNode;
			antes.next = temp;
		}
		if(actualNode != null && actualNode.next == null)
		{
			lastNode = actualNode;
		}
		size++;
		return elem;
	}
	
	public Node merge(Node<T> first, Node<T> second)
	{
		if (first == null) return second;
	    if (second == null) return first;

	    if (first.getElement().compareTo(second.getElement()) < 0) {
	        first.next = merge(first.next, second);
	        return first;
	    } else {
	        second.next = merge(second.next, first);
	        return second;
	    }
	}
	
	public void avanzar()
	{
		actualNode = actualNode.next;
	}
	
	
	public int size() {
		return size;
	}

	@Override
	public T get(int position) {
		listing();
		// TODO Auto-generated method stub
		if(position+1 > size)
			return null;
		while(actualNode.getNext() != null && position != 0){
			actualNode = actualNode.getNext();
			position--;
		}
		return actualNode.getElement();
	}
	@Override
	public void listing() {
		// TODO Auto-generated method stub
		actualNode = first;
	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return actualNode.getElement();
	}
	
	public T getLast() {
		return lastNode.getElement();
	}

	@Override
	public Node<T> next() {
		// TODO Auto-generated method stub
		return actualNode.getNext();
	}
	
	
}
